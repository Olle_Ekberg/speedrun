//
//  PlayVideo.swift
//  Speedrun
//
//  Created by Olle Ekberg on 2017-08-05.
//  Copyright © 2017 Olle Ekberg. All rights reserved.
//

import Foundation
import UIKit
import AVFoundation

class PlayVideo: UIWebView
{
 
    var alertController = MyAlertController()
    
    // embed video in webview
    func loadVideo(videoURL: String,webView: UIWebView,completion: @escaping()->())
    {
        if videoURL.lowercased().range(of:"www.youtube.com") != nil
        {
            print("youtube")
            let replace1 = videoURL.replacingOccurrences(of: "https://www.youtube.com/watch?v=", with: "")
            let replace2 = replace1.replacingOccurrences(of: "&feature=youtu.be", with: "")
            let videoLink = URL(string: "https://www.youtube.com/embed/\(replace2)?&playsinline=1") // youtube
            webView.loadRequest(URLRequest(url: videoLink!))
        }else if videoURL.lowercased().range(of: "https://youtu.be") != nil
        {
            print("youtube")
            let replace1 = videoURL.replacingOccurrences(of: "https://youtu.be/", with: "")
            let videoLink = URL(string: "https://www.youtube.com/embed/\(replace1)?&playsinline=1") // youtube
            webView.loadRequest(URLRequest(url: videoLink!))
        }else if videoURL.lowercased().range(of: "www.twitch.tv") != nil
        {
            print("twitch")
            let videoLink = URL(string : videoURL)
            webView.loadRequest(URLRequest(url: videoLink!))
        }else{
            completion()
        }
    }
}

/*else if videoURL.lowercased().range(of: "www.twitch.tv") != nil{
    let replace1 = videoURL.replacingOccurrences(of: "https://www.twitch.tv/dk28/c/", with: "")
    let videoLink = URL(string : "https://player.twitch.tv/?video=v\(replace1)&autoplay=false")
    webView.loadRequest(URLRequest(url: videoLink!))
}*/
