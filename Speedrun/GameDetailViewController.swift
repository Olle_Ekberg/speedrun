//
//  GameDetailViewController.swift
//  Speedrun
//
//  Created by Olle Ekberg on 2017-06-27.
//  Copyright © 2017 Olle Ekberg. All rights reserved.
//

import UIKit

class GameDetailViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var categoriesTableView: UITableView!
    @IBOutlet weak var coverImage: UIImageView!
    @IBOutlet weak var loadingView: UIView!
    
    var categories: NSArray?
    var getCover = getImage()
    var alertController = MyAlertController()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        getCover.getImage(imageURL: PlayerAndTime.sharedInstance.urlCoverImage, coverImage: coverImage)
        loadTheCategories()
        
        // Do any additional setup after loading the view.
    }

    //number of cells
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if(categories == nil)
        {
            return 0
        }
        return categories!.count
    }
    
    //what a cell looks like
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "game detail cell", for: indexPath) as! GameDetailTableViewCell
        
        cell.gameDetailLabel.text = "Categories"
        
        let currentCategory = categories![indexPath.row] as! NSDictionary
        let name = currentCategory.object(forKey: "name") as! String

        cell.gameDetailLabel.text = name
        
        return cell
        
    }
    
    //pressing a row
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let category = categories![indexPath.row] as! NSDictionary
        PlayerAndTime.sharedInstance.categoryName = category.object(forKey: "name") as! String
        
        let links = category.object(forKey: "links") as! NSArray
        let recordCategory = links[3] as! NSDictionary
        PlayerAndTime.sharedInstance.urlLeaderboard = recordCategory.object(forKey: "uri") as! String
        
        performSegue(withIdentifier: "leaderboard segue", sender: self)
    }
    
    //search JSON
    func loadTheCategories()
    {
        loadingView.isHidden = false
        
        let request = NSMutableURLRequest(url: URL(string: PlayerAndTime.sharedInstance.urlCategory)!)
        let session = URLSession.shared
        request.httpMethod = "GET"
        
        let task = session.dataTask(with: request as URLRequest, completionHandler: {data, response, error -> Void in
           // print("Response: \(response)")
           // print(error.debugDescription)
            if(error != nil)
            {
                print("ERROR ERROR!!")
                DispatchQueue.main.async {
                    self.loadingView.isHidden = true
                    
                    self.present(self.alertController.twoAction(ftitle: "Error!!", fmessage: "Couldn't get categories", firstActionTitle: "Try Again!", secondActionTitle: "Cancel", completion: {
                        
                        self.loadTheCategories()
                    }), animated: true, completion: nil)
                }
                return
            }
            /*let strData = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
            print("Body: \(String(describing: strData))")*/
            
            do {
                let json = try JSONSerialization.jsonObject(with: data!, options: .mutableLeaves) as? NSDictionary
                
                self.categories = json?.object(forKey: "data") as? NSArray

                print("CATEGORIES COUNT!!!")
                print(self.categories!.count)
                
                DispatchQueue.main.async {
                    self.categoriesTableView.reloadData()
                    self.loadingView.isHidden = true
                }
                
            } catch {
                print("ERROR call")
                
                DispatchQueue.main.async {
                    self.loadingView.isHidden = true
                    
                    self.present(self.alertController.twoAction(ftitle: "Error!!", fmessage: "Couldn't get categories", firstActionTitle: "Try Again!", secondActionTitle: "Cancel", completion: {
                        
                        self.loadTheCategories()
                    }), animated: true, completion: nil)
                }
            }
        }) // let task
        task.resume()
    }
    
    //back button
    @IBAction func backButton(_ sender: Any)
    {
        self.presentingViewController?.dismiss(animated: true, completion: nil)
    }
    
    //color on status bar
    override var preferredStatusBarStyle: UIStatusBarStyle
    {
        return .lightContent
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
