//
//  VideoViewController.swift
//  Speedrun
//
//  Created by Olle Ekberg on 2017-08-05.
//  Copyright © 2017 Olle Ekberg. All rights reserved.
//

import UIKit

class VideoViewController: UIViewController, UIWebViewDelegate {
    
    @IBOutlet weak var webView: UIWebView!
    @IBOutlet weak var categoryLabel: UILabel!
    @IBOutlet weak var commentsTextView: UITextView!
    @IBOutlet weak var loadingView: UIView!
    var playVideo = PlayVideo()
    var alertController = MyAlertController()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        commentsTextView.text = PlayerAndTime.sharedInstance.gameComment!
        categoryLabel.text = PlayerAndTime.sharedInstance.categoryName
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        loadingView.isHidden = false
        webView.delegate = self
        playVideo.loadVideo(videoURL: PlayerAndTime.sharedInstance.playVideoURL!, webView: webView, completion: {
            self.present(self.alertController.oneAction(ftitle: "Darn!", fmessage: "This video doesn't exist or it must have some weird URL", actionTitle: "OK", completion: {
                self.presentingViewController?.dismiss(animated: true, completion: nil)
            }), animated: true, completion: nil)
        })
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        loadingView.isHidden = true
    }
    
    @IBAction func backButton(_ sender: Any) {
        self.presentingViewController?.dismiss(animated: true, completion: nil)
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
