//
//  PlayerAndTime.swift
//  Speedrun
//
//  Created by Olle Ekberg on 2017-06-30.
//  Copyright © 2017 Olle Ekberg. All rights reserved.
//

import Foundation

class PlayerAndTime
{
    static var sharedInstance = PlayerAndTime()
    
    var playerTime = ""
    var playerTimeInt : Float!
    var playerJson = ""
    var searchText = ""
    var urlCategory = ""
    var urlCoverImage = ""
    var urlFirstTrophyImage = ""
    var urlSecondTrophyImage = ""
    var urlThirdTrophyImage = ""
    var categoryName = ""
    var urlLeaderboard = ""
    var names = [String]()
    var Jsons = [String]()
    var videoURL = [String]()
    var playVideoURL : String?
    var gameComments = [String]()
    var gameComment : String?
    
    func getName(completion: @escaping()->())
    {
        let request = NSMutableURLRequest(url: URL(string: playerJson)!)
        let session = URLSession.shared
        request.httpMethod = "GET"

        let task = session.dataTask(with: request as URLRequest, completionHandler: {data, response, error -> Void in

            /*let strData = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
            print("Body: \(strData)")*/
            
            do {
                let json = try JSONSerialization.jsonObject(with: data!, options: .mutableLeaves) as? NSDictionary
                let playerDictionary = json?.object(forKey: "data") as? NSDictionary
                    
                if (playerDictionary == nil)
                {
                    self.names.append("N/A(Guest)")
                }else if let name = playerDictionary!.object(forKey: "name")
                {
                    self.names.append(name as! String+"(Guest)")
                }else if let names = playerDictionary!.object(forKey: "names") as? NSDictionary
                {
                    let international = names.object(forKey: "international") as! String
                    //self.playerName = international
                    self.names.append(international)
                }
            } catch {
                print("ERROR call")
            }
            completion()
            
            }) // let task
            task.resume()
        }
}
