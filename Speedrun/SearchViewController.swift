//
//  SearchViewController.swift
//  Speedrun
//
//  Created by Olle Ekberg on 2017-06-27.
//  Copyright © 2017 Olle Ekberg. All rights reserved.
//

import UIKit

class SearchViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var searchTextField: UITextField!
    @IBOutlet weak var clearButton: UIButton!
    @IBOutlet weak var searchButton: UIButton!

    var alertController = MyAlertController()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        searchTextField.becomeFirstResponder()
        
        // Do any additional setup after loading the view.
    }

    //Pressing Enter key
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        view.endEditing(true)
        
        return true
    }
    
    //searchbutton
    @IBAction func searchGame(_ sender: Any)
    {
        if searchTextField.text == ""
        {
            self.present(alertController.oneAction(ftitle: "HEY! LISTEN!", fmessage: "You need to write something in the search field!", actionTitle: "OK", completion: {
              print("skriv något")
            }), animated: true, completion: nil)
        }else
        {
            // saving serach to sharedInstance
            PlayerAndTime.sharedInstance.searchText = searchTextField.text!
            performSegue(withIdentifier: "searchSegue", sender: self)
            searchTextField.text = ""
        }
    }
    
    //clearbutton
    @IBAction func clearText(_ sender: Any)
    {
        searchTextField.text = ""
    }
    
    //color on status bar
    override var preferredStatusBarStyle: UIStatusBarStyle
    {
        return .lightContent
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
}
