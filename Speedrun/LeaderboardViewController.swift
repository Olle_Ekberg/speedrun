//
//  LeaderboardViewController.swift
//  Speedrun
//
//  Created by Olle Ekberg on 2017-06-27.
//  Copyright © 2017 Olle Ekberg. All rights reserved.
//

import UIKit

class LeaderboardViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var leaderboardTableView: UITableView!
    @IBOutlet weak var categoryLabel: UILabel!
    @IBOutlet weak var coverImage: UIImageView!
    @IBOutlet weak var loadingView: UIView!
    @IBOutlet weak var newSearchButton: StandardButton!

    let formatter = DateFormatter()
    var dateTime = Date()
    var playerID = ""
    var playerTimeArray = [PlayerAndTime]()
    var runNr = 0
    var getCover = getImage()
    var alertController = MyAlertController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        getCover.getImage(imageURL: PlayerAndTime.sharedInstance.urlCoverImage, coverImage: coverImage)

        //adaptable cell size
        leaderboardTableView.rowHeight = UITableViewAutomaticDimension
        leaderboardTableView.estimatedRowHeight = 140

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        categoryLabel.text = PlayerAndTime.sharedInstance.categoryName
        if runNr == 0
        {
        loadTheLeaderboard()
        }
    }
    
    //number of cells
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        if (playerTimeArray == nil)
        {
            return 0
        }
        return playerTimeArray.count
    }
    
    //what a cell looks like
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "leaderboard cell", for: indexPath) as! LeaderboardTableViewCell
        
        cell.timeLabel.text = "LOADING"
        cell.nameLabel.text = ""
        cell.nameLabel.sizeToFit()
        if (playerTimeArray == nil)
        {
            print("Inga namn eller tider")
        }else
        {
            for i in playerTimeArray[indexPath.row].names
            {
                if (cell.nameLabel.text == "")
                {
                    cell.nameLabel.text = i
                }else
                {
                    cell.nameLabel.text = cell.nameLabel.text! + "\n" + i
                }
            }
            let gameTime = playerTimeArray[indexPath.row].playerTime
            
            //removing letters
            let newString = gameTime.replacingOccurrences(of: "PT", with: "", options: .literal, range: nil)
            let HString = newString.replacingOccurrences(of: "H", with: ":", options: .literal, range: nil)
            let MString = HString.replacingOccurrences(of: "M", with: ":", options: .literal, range: nil)
            let SString = MString.replacingOccurrences(of: "S", with: "", options: .literal, range: nil)
            
            cell.timeLabel.text = "Time: "+SString
        }
        
        // first row in tableview
        if (indexPath.row == 0)
        {
            cell.placeLabel.text = "1st"
            //cell.trophyImage.image = #imageLiteral(resourceName: "goldtrophy")
            cell.placeImage.image = #imageLiteral(resourceName: "Gold Color Line Dark")
            getCover.getImage(imageURL: PlayerAndTime.sharedInstance.urlFirstTrophyImage, coverImage: cell.trophyImage)
            
        // second row in tableview
        }else if (indexPath.row == 1)
        {
            cell.placeLabel.text = "2nd"
            //cell.trophyImage.image = #imageLiteral(resourceName: "silvertrophy")
            cell.placeImage.image = #imageLiteral(resourceName: "Silver Color Line Dark")
            getCover.getImage(imageURL: PlayerAndTime.sharedInstance.urlSecondTrophyImage, coverImage: cell.trophyImage)
            
        // third row in tableview
        }else if (indexPath.row == 2)
        {
            cell.placeLabel.text = "3rd"
            //cell.trophyImage.image = #imageLiteral(resourceName: "bronzetrophy")
            cell.placeImage.image = #imageLiteral(resourceName: "Bronze Color Line Dark")
            getCover.getImage(imageURL: PlayerAndTime.sharedInstance.urlThirdTrophyImage, coverImage: cell.trophyImage)
            
        // if more rows exist in tableview
        }else
        {
            cell.placeLabel.text = "EXTRA"
        }
        return cell
    }
    
    //pressing a row
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        for element in playerTimeArray {
            print(element.names)
            print(element.videoURL)
        }
        
        print(playerTimeArray[indexPath.row].videoURL)
        
        for i in playerTimeArray[indexPath.row].videoURL
        {
            PlayerAndTime.sharedInstance.playVideoURL = i
        }
        
        for c in playerTimeArray[indexPath.row].gameComments{
            PlayerAndTime.sharedInstance.gameComment = c
        }
        
        print(PlayerAndTime.sharedInstance.playVideoURL!)
        
        performSegue(withIdentifier: "showvideosegue", sender: self)
    }
    
    //back button
    @IBAction func BackButton(_ sender: Any) {
        self.presentingViewController?.dismiss(animated: true, completion: nil)
    }
    
    //new search button
    @IBAction func newSearch(_ sender: Any) {
        self.presentingViewController?.presentingViewController?.presentingViewController?.dismiss(animated: true, completion: nil)
    }
    
    //search JSON
    func loadTheLeaderboard()
    {
        loadingView.isHidden = false
        
        let request = NSMutableURLRequest(url: URL(string: PlayerAndTime.sharedInstance.urlLeaderboard)!)
        let session = URLSession.shared
        request.httpMethod = "GET"
        
        let task = session.dataTask(with: request as URLRequest, completionHandler: {data, response, error -> Void in
            // print("Response: \(response)")
            //print(error.debugDescription)
            if(error != nil)
            {
                print("ERROR ERROR!!")
                DispatchQueue.main.async {
                    self.loadingView.isHidden = true
                    
                    self.present(self.alertController.twoAction(ftitle: "Error!!", fmessage: "Couldn't get records", firstActionTitle: "Try again!", secondActionTitle: "Cancel", completion: {
                        
                        self.loadTheLeaderboard()
                    }), animated: true, completion: nil)
                }
                return
            }
           /* let strData = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
            print("Body: \(String(describing: strData))")*/
            
            do {
                let json = try JSONSerialization.jsonObject(with: data!, options: .mutableLeaves) as? NSDictionary

                let temp = json?.object(forKey: "data") as! NSArray
                let temp2 = temp[temp.count-1] as! NSDictionary
                let leaderboardArray = temp2.object(forKey: "runs") as! NSArray
                
                if (leaderboardArray.count == 0)
                {
                    DispatchQueue.main.async {
                        self.loadingView.isHidden = true
                        
                        self.present(self.alertController.oneAction(ftitle: "WHAT!!??", fmessage: "There are no records for this category. This is your chance!", actionTitle: "Back", completion: {
                            self.presentingViewController?.dismiss(animated: true, completion: nil)
                        }), animated: true, completion: nil)
                    }
                }else
                {
                for index in leaderboardArray
                {
                    var comment : String!
                    var videoURL : String!
                    var primaryTimeInt : Float!
                    
                    let playerObject = PlayerAndTime()
                    let currentLeader = index as! NSDictionary
                    let run = currentLeader.value(forKey: "run") as! NSDictionary
                    let times = run.object(forKey: "times") as! NSDictionary
                    let primaryTime = times.object(forKey: "primary") as! String
                    if(times.object(forKey: "primary_t") is NSNull){
                        primaryTimeInt = nil
                    }else{
                        primaryTimeInt = times.object(forKey: "primary_t") as! Float
                    }
                    let players = run.object(forKey: "players") as! NSArray
                    if(run.object(forKey: "videos") is NSNull){
                        videoURL = ""
                        comment = ""
                    }else{
                        let videos = run.object(forKey: "videos") as! NSDictionary
                        let videoLink = videos.object(forKey: "links") as! NSArray
                        let videoLinkDic = videoLink[videoLink.count-1] as! NSDictionary
                        videoURL = videoLinkDic.object(forKey: "uri") as! String
                        if(run.object(forKey: "comment") is NSNull){
                            comment = "Gamer had no comments!"
                        }else{
                            comment = run.object(forKey: "comment") as! String
                        }
                    }
                    for index in players
                    {
                        let player = index as! NSDictionary
                        let uri = player.object(forKey: "uri")
                        //playerObject.Jsons.append(uri as! String)
                        
                        playerObject.playerJson = uri as! String
                        playerObject.getName(completion:
                            
                            {
                                DispatchQueue.main.async
                                    {
                                        self.runNr = 1
                                        self.leaderboardTableView.reloadData()
                                        self.loadingView.isHidden = true
                                }
                        })
                    }
                    if comment == nil{
                        playerObject.gameComments.append("Gamer had no comments!")
                    }else{
                        playerObject.gameComments.append(comment!)
                    }
                    playerObject.videoURL.append(videoURL)
                    playerObject.playerTime = primaryTime
                    playerObject.playerTimeInt = primaryTimeInt
                    self.playerTimeArray.append(playerObject)
                }
                }
            } catch {
                print("ERROR call")
                
                DispatchQueue.main.async {
                    self.loadingView.isHidden = true
                    
                    self.present(self.alertController.twoAction(ftitle: "Error!!", fmessage: "Couldn't get records", firstActionTitle: "Try again!", secondActionTitle: "Cancel", completion: {
                        self.loadTheLeaderboard()
                    }), animated: true, completion: nil)
                }
            }
        })
        task.resume()
    }
    
    //color on status bar
    override var preferredStatusBarStyle: UIStatusBarStyle
    {
        return .lightContent
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
