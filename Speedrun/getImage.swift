//
//  getImage.swift
//  Speedrun
//
//  Created by Olle Ekberg on 2017-07-28.
//  Copyright © 2017 Olle Ekberg. All rights reserved.
//

import Foundation
import UIKit

class getImage: UIImageView
{
    //get cover image
    func getImage(imageURL: String, coverImage: UIImageView)
    {
        guard let url = URL(string: imageURL) else { return }
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            if error != nil {
                print("Failed fetching image:", error!)
                return
            }
            guard let response = response as? HTTPURLResponse, response.statusCode == 200 else {
                print("Not a proper HTTPURLResponse or statusCode")
                return
            }
            DispatchQueue.main.async {
                coverImage.image = UIImage(data: data!)
                print("IMAGE DONE")
            }
        }.resume()
    }
}
