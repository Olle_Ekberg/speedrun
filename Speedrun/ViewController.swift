//
//  ViewController.swift
//  Speedrun
//
//  Created by Olle Ekberg on 2017-06-26.
//  Copyright © 2017 Olle Ekberg. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var gamesTableView: UITableView!
    @IBOutlet weak var searchLabel: UILabel!
    @IBOutlet weak var loadingView: UIView!
    
    var games : NSArray?
    var alertController = MyAlertController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        searchLabel.text = PlayerAndTime.sharedInstance.searchText
        loadTheGames()
    }
    
    // number of rows
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if(games == nil)
        {
            return 0
        }
        return games!.count
    }
    
    //how a cell looks
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "games cell", for: indexPath) as! GamesTableViewCell
        //cell.cellLabel.text = "GATEGORY"
        let currentGame = games![indexPath.row] as! NSDictionary
        let name = currentGame.object(forKey: "names") as! NSDictionary
        let international = name.object(forKey: "international") as! String
        cell.cellLabel.text = international
        return cell
    }
    
    // pressing a row
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        
        //Get game Json, save to shearedInstance
        let currentGame = games![indexPath.row] as! NSDictionary
        let links = currentGame.object(forKey: "links") as! NSArray
        let categories = links[3] as! NSDictionary
        PlayerAndTime.sharedInstance.urlCategory = categories.object(forKey: "uri") as! String
        
        //Get coverImage URL, save to shearedInstance
        let assets = currentGame.object(forKey: "assets") as! NSDictionary
        let cover = assets.object(forKey: "cover-large") as! NSDictionary
        PlayerAndTime.sharedInstance.urlCoverImage = cover.object(forKey: "uri") as! String
        
        //Get trophyImages URL, save to shearedInstance
        let first = assets.object(forKey: "trophy-1st") as! NSDictionary
        PlayerAndTime.sharedInstance.urlFirstTrophyImage = first.object(forKey: "uri") as! String
        let second = assets.object(forKey: "trophy-2nd") as! NSDictionary
        PlayerAndTime.sharedInstance.urlSecondTrophyImage = second.object(forKey: "uri") as! String
        let third = assets.object(forKey: "trophy-3rd") as! NSDictionary
        PlayerAndTime.sharedInstance.urlThirdTrophyImage = third.object(forKey: "uri") as! String
        
        performSegue(withIdentifier: "gameDetail", sender: self)
    }
    
    //backbutton
    @IBAction func backButton(_ sender: Any)
    {
        self.presentingViewController?.dismiss(animated: true, completion: nil)
    }
    
    
    // search the JSON
    func loadTheGames()
     {
     loadingView.isHidden = false
        
     let urlGame = "http://www.speedrun.com/api/v1/games?name="
     let newString = PlayerAndTime.sharedInstance.searchText.replacingOccurrences(of: " ", with: "%20", options: .literal, range: nil)
        
     let request = NSMutableURLRequest(url: URL(string: urlGame+newString)!)
     let session = URLSession.shared
     request.httpMethod = "GET"
     
     let task = session.dataTask(with: request as URLRequest, completionHandler: {data, response, error -> Void in
    // print("Response: \(response)")
    // print(error.debugDescription)
     if(error != nil)
        {
        print("ERROR ERROR!!")
        DispatchQueue.main.async {
            self.loadingView.isHidden = true
     
            self.present(self.alertController.twoAction(ftitle: "Error!!", fmessage: "Couldn't get games", firstActionTitle: "Try Again!", secondActionTitle: "Cancel", completion: {
                
                self.loadTheGames()
                }), animated: true, completion: nil)
            }
            return
        }
        
     /*let strData = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
     print("Body: \(String(describing: strData))")*/
     
     do {
        let json = try JSONSerialization.jsonObject(with: data!, options: .mutableLeaves) as? NSDictionary
     
        self.games = json?.object(forKey: "data") as? NSArray

        print("GAMES COUNT!!!!")
        print(self.games!.count)
     
        DispatchQueue.main.async {
            self.gamesTableView.reloadData()
            self.loadingView.isHidden = true
        }
     
     } catch {
        print("ERROR call")
        
        self.loadingView.isHidden = true
        
        self.present(self.alertController.twoAction(ftitle: "Error!!", fmessage: "Couldn't get games", firstActionTitle: "Try Again!", secondActionTitle: "Cancel", completion: {
            
            self.loadTheGames()
            }), animated: true, completion: nil)
        }
     }) 
     task.resume()
     }
        
    //color on status bar
    override var preferredStatusBarStyle: UIStatusBarStyle
    {
        return .lightContent
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

