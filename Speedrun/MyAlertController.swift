//
//  MyAlertController.swift
//  Speedrun
//
//  Created by Olle Ekberg on 2017-08-01.
//  Copyright © 2017 Olle Ekberg. All rights reserved.
//

import Foundation
import UIKit

class MyAlertController: UIAlertController
{
    var alertController : UIAlertController?
    var oneAction : UIAlertAction?
    var twoAction : UIAlertAction?
    
    func oneAction(ftitle : String, fmessage: String, actionTitle : String, completion: @escaping () -> ()) -> UIAlertController
    {
        alertController = UIAlertController()
        alertController?.title = ftitle
        alertController?.message = fmessage
        oneAction = UIAlertAction(title: actionTitle, style: .default){ _ in
            completion()
        }
        alertController?.addAction(oneAction!)
        return alertController!
    }
    
    func twoAction(ftitle : String, fmessage: String, firstActionTitle : String, secondActionTitle : String, completion: @escaping () -> ()) -> UIAlertController
    {
        alertController = UIAlertController()
        alertController?.title = ftitle
        alertController?.message = fmessage
        oneAction = UIAlertAction(title: firstActionTitle, style: .destructive){_ in
            completion()
        }
        twoAction = UIAlertAction(title: secondActionTitle, style: .cancel){_ in
            return
        }
        alertController?.addAction(oneAction!)
        alertController?.addAction(twoAction!)
        return alertController!
    }
}
