//
//  GameDetailTableViewCell.swift
//  Speedrun
//
//  Created by Olle Ekberg on 2017-06-27.
//  Copyright © 2017 Olle Ekberg. All rights reserved.
//

import UIKit

class GameDetailTableViewCell: UITableViewCell {
    
    @IBOutlet weak var gameDetailLabel: UILabel!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
